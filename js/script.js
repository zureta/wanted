$(function(){
    
    $(window).scroll(function() {
        
        var shrinkHeader = 1,
            scroll = getCurrentScroll(),
            rotate = 360,
            unrotate = 0;

        if ( scroll >= shrinkHeader ) {
            $('.nav').addClass('sticky');
            $('.logo').css({
                '-moz-transform': 'rotateZ(' + rotate + 'deg)',
                '-webkit-transform': 'rotateZ(' + rotate + 'deg)',
                '-o-transform': 'rotateZ(' + rotate + 'deg)',
                '-ms-transform': 'rotateZ(' + rotate + 'deg)',
                'transform': 'rotateZ(' + rotate + 'deg)',
            });
        }
        else {
            $('.logo').css({
                '-moz-transform': 'rotateZ(' + unrotate + 'deg)',
                '-webkit-transform': 'rotateZ(' + unrotate + 'deg)',
                '-o-transform': 'rotateZ(' + unrotate + 'deg)',
                '-ms-transform': 'rotateZ(' + unrotate + 'deg)',
                'transform': 'rotateZ(' + unrotate + 'deg)',
            });
            $('.nav').removeClass('sticky');
        }

    });

    function getCurrentScroll() {
        return window.pageYOffset || document.documentElement.scrollTop;
    }

    /* Animations on scroll  */

    $('.js--wp-1').waypoint(function(direction){
        $('.js--wp-1').addClass('animated fadeIn');
     },{
         offset:'50%'
     });

     $('.js--wp-2').waypoint(function(direction){
        $('.js--wp-2').addClass('animated fadeInUp');
     },{
         offset:'50%'
     });

     $('.js--wp-3').waypoint(function(direction){
        $('.js--wp-3').addClass('animated fadeIn');
     },{
         offset:'50%'
     });
});